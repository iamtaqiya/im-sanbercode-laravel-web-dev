<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Film extends Model
{
    use HasFactory;

    protected $table = 'film';

    // fillable = bisa manipulasi kolomnya / kolom apa saja yg mau dimanipulasi (tambah, update, edit, delete, dll)
    protected $fillable = ['judul', 'ringkasan', 'tahun', 'poster', 'genre_id'];
    // ada genre_id, maka butuh Model Genre

}
