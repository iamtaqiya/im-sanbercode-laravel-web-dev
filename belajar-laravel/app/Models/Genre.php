<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Genre extends Model
{
    use HasFactory;

    protected $table = 'genre';

    // fillable = bisa manipulasi kolomnya / kolom apa saja yg mau dimanipulasi (tambah, update, edit, delete, dll)
    protected $fillable = ['nama'];
}
