<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Profil extends Model
{
    use HasFactory;

    protected $table = 'profil';

    // fillable = bisa manipulasi kolomnya / kolom apa saja yg mau dimanipulasi (tambah, update, edit, delete, dll)
    protected $fillable = ['umur', 'bio', 'alamat', 'users_id'];
    // ada users_id, maka butuh Model User?

    public function users() //users = nama fungsi yg dibuat, biasanya aku samain nama table
    {
        return $this->belongsTo(User::class, 'users_id'); //User = model
    }
}

//hasOne = dipakai kalo tabel tidak ada FOREIGN KEY
//belongsTo = dipakai kalo tabel ada FOREIGN KEY