<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Genre;
use App\Models\Film;
use File; //untuk menghapus filenya

class FilmController extends Controller
{
    // agar tidak bisa akses 'index' & 'show'
    public function __construct()
    {
        $this->middleware('auth')->except(['index', 'show']);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $genre = Genre::all();
        return view('film.create', ['genre' => $genre]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'judul' => 'required',
            'ringkasan' => 'required',
            'tahun' => 'required',
            'poster' => 'required|mimes:png,jpg,jpeg|max:2048',
            'genre_id' => 'required',
        ]);

        // supaya nama file unik, tidak replace
        $poster_nama = $request->judul . time().'.'.$request->poster->extension();

        $request->poster->move(public_path('Poster'), $poster_nama);

        // menambahkan/menyimpan data ke database. Ini caranya hampir mirip kayak intansi object di dalam OOP
        $film = new Film;   // ---> variable = new Nama Object/Class
        
        $film->judul = $request['judul']; // variable->propertynya apa saja = ambil nilainya apa
        $film->ringkasan = $request['ringkasan'];
        $film->tahun = $request['tahun'];
        $film->poster = $poster_nama;
        $film->genre_id = $request['genre_id'];
        
        $film->save();

        return redirect('/film');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $film = Film::all();
        return view('film.index', ['film' => $film]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $film = Film::find($id);
        return view('film.show', ['film' => $film]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $film = Film::find($id);
        $genre = Genre::all();
        return view('film.edit', ['genre' => $genre, 'film' => $film]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'judul' => 'required',
            'ringkasan' => 'required',
            'tahun' => 'required',
            'poster' => 'mimes:png,jpg,jpeg|max:2048',
            'genre_id' => 'required',
        ]);

        $film = Film::find($id);

        if($request->has('poster')) {
            // hapus dulu file yg lama
            $path = 'Poster/';
            File::delete($path . $film->poster);

            $poster_nama = $request->judul . time().'.'.$request->poster->extension();

            $request->poster->move(public_path('Poster'), $poster_nama);
            
            $film->poster = $poster_nama;
        }

        $film->judul = $request['judul']; // variable->propertynya apa saja = ambil nilainya apa
        $film->ringkasan = $request['ringkasan'];
        $film->tahun = $request['tahun'];
        $film->genre_id = $request['genre_id'];
        
        $film->save();

        return redirect('/film');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $film = Film::find($id);

        // hapus dulu file yg lama
        $path = 'Poster/';
        File::delete($path . $film->poster);

        $film->delete();

        return redirect('/film');
    }
}