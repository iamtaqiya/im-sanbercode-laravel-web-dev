<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function dua()
    {
        return view("link.2-register");
    }
    
    public function tiga(Request $request)
    {
        $namaDepan = $request->input("fname");
        $namaBelakang = $request->input("lname");

        return view("link.3-welcome", ["namaDepan"=>$namaDepan, "namaBelakang"=>$namaBelakang]);
    }
}