<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class GameController extends Controller
{
    public function create() 
    {
        return view('game.create');
    }

    public function store(Request $request)
    {
        // validasi
        $request->validate([
            'name' => 'required',
            'gameplay' => 'required',
            'developer' => 'required',
            'year' => 'required',
        ]);

        // insert data ke database
        DB::table('game')->insert([
            'name' => $request['name'],
            'gameplay' => $request['gameplay'],
            'developer' => $request['developer'],
            'year' => $request['year'],
        ]);

        return redirect('/game');
    }

    public function index()
    {
        $game = DB::table('game')->get();
 
        return view('game.index', ['game' => $game]);
    }

    public function show($id)
    {
        $gameData = DB::table('game')->find($id);
 
        return view('game.show', ['gameData' => $gameData]);
    }

    public function edit($id)
    {
        $gameData = DB::table('game')->find($id);

        return view('game.edit', ['gameData' => $gameData]);
    }

    public function update(Request $request, $id)
    {
        // validasi
        $request->validate([
            'name' => 'required',
            'gameplay' => 'required',
            'developer' => 'required',
            'year' => 'required',
        ]);

        // update data ke database
        DB::table('game')
            ->where('id', $id)
            ->update(
                [
                'name' => $request['name'],
                'gameplay' => $request['gameplay'],
                'developer' => $request['developer'],
                'year' => $request['year'],
                ],
            );
        
        return redirect('/game');
    }

    public function destroy($id)
    {
        DB::table('game')->where('id', '=', $id)->delete();

        return redirect('/game');
    } 
}