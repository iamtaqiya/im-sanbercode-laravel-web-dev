<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Models\Profil;

class ProfilController extends Controller
{
    public function index()
    {
        //cara ambil user yg login
        // Retrieve the currently authenticated user's ID...
        $userid = Auth::id();

        $profil = Profil::where('users_id', $userid)->first();

        return view('profil.edit', ['profil' => $profil]); //[] artinya kita passing
    }

    public function update(Request $request, $id)
    {
        // validasi
        $request->validate([
            'umur' => 'required|integer',
            'bio' => 'required',
            'alamat' => 'required',
        ]);

        $profil = Profil::find($id);
 
        $profil->umur = $request['umur'];
        $profil->bio = $request['bio'];
        $profil->alamat = $request['alamat'];
 
        $profil->save();

        return redirect('/profil');
    }
}
