@extends("layouts.master")

@section("judul")
    Semua Genre
@endsection

@section("content")

<a href="/genre/create" class="btn btn-primary btn-sm my-3">Tambah</a>

<table class="table">
    <thead class="thead-dark">
        <tr>
            <th scope="col">#</th>
            <th scope="col">Nama Genre</th>
            <th scope="col">Action</th>
        </tr>
    </thead>
    
    <tbody>
        @forelse ($genre as $key => $item)
            <tr>
                <th scope="row">{{$key + 1}}</th>
                <td>{{$item->nama}}</td>
                <td>
                    <form action="/genre/{{$item->id}}" method="POST">
                    @csrf
                    @method('DELETE')
                    <a href="/genre/{{$item->id}}" class="btn btn-info btn-sm mx-1">Detail</a>
                    <a href="/genre/{{$item->id}}/edit" class="btn btn-warning btn-sm mx-1">Edit</a>
                    <input type="submit" class="btn btn-danger btn-sm mx-1" value="Delete">
                    </form>
                </td>
            </tr>
        @empty
            <tr>
                <td>Belum Ada Genre</td>
            </tr>
        @endforelse
    </tbody>
  </table>

@endsection