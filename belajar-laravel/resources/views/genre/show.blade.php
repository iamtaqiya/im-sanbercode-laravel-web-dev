@extends("layouts.master")

@section("judul")
    Detail Genre
@endsection

@section("content")

<h1 class="text-white text-center bg-primary">{{$genreData->nama}}</h1>

<a href="/genre" class="btn btn-secondary btn-sm">Kembali</a>

<div class="card text-center">
    <div class="card-body">
        {{-- <h4 class="text-primary">{{$filmData->judul}}</h4> --}}
        judul
        poster
        tahun
        ringkasan
        <p>{{$genreData->nama}}</p>
        nama cast

        <a href="#" class="btn btn-secondary btn-sm mt-5">Ulasan</a>
    </div>
</div>
@endsection