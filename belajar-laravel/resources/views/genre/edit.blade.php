@extends("layouts.master")

@section("judul")
    Edit Genre
@endsection

@section("content")

<form action="/genre/{{$genreData->id}}" method="POST">
    @csrf
    @method('PUT')

    <div class="form-group">
        <label>Nama Genre</label>
        <input type="text" name="nama" value="{{$genreData->nama}}" class="form-control">
    </div>

    @error('nama')
    <div class="alert alert-danger">{{ $message }}</div>
    @enderror

    <button type="submit" class="btn btn-primary">Edit</button>
</form>

@endsection