<!doctype html>

<html lang="en">

<head>

<!-- Required meta tags -->

<meta charset="utf-8">

<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

<!-- Bootstrap CSS -->

<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.1/dist/css/bootstrap.min.css" integrity="sha384-zCbKRCUGaJDkqS1kPbPd7TveP5iyJE0EjAuZQTgFLD2ylzuqKfdKlfG/eSrtxUkn" crossorigin="anonymous">

<title>Table Data</title>

</head>

<body class="container-fluid">

<h2 class="my-5">List Game</h2>

<a href="/game/create" class="btn btn-primary mb-2">Tambah</a>

{{-- //isi link a href menuju ke form tambah data --}}

<table class="table text-center">

<thead class="thead-dark">

<tr>

<th scope="col">#</th>

<th scope="col">Name</th>

<th scope="col">Gameplay</th>

<th scope="col">Developer</th>

<th scope="col">Year</th>

<th scope="col" >Actions</th>

</tr>

</thead>

<tbody>

{{-- //code di sini tampilkan semua data di database dan buat link dan tombol untuk edit, detail, dan delete --}}

@forelse ($game as $key => $item)
    <tr>
        <th scope="row">{{$key + 1}}</th>
        <td>{{$item->name}}</td>
        <td>{{$item->gameplay}}</td>
        <td>{{$item->developer}}</td>
        <td>{{$item->year}}</td>
        <td>
            <form action="/game/{{$item->id}}" method="POST">
            @csrf
            @method('DELETE')
            <input type="submit" class="btn btn-danger btn-sm mx-1" value="Delete">
            <a href="/game/{{$item->id}}/edit" class="btn btn-warning btn-sm mx-1">Edit</a>
            <a href="/game/{{$item->id}}" class="btn btn-info btn-sm mx-1">Detail</a>
            </form>
        </td>
    </tr>
@empty
    <tr>
        <td>Belum Ada Game</td>
    </tr>
@endforelse

</tbody>

</table>




<script src="https://cdn.jsdelivr.net/npm/jquery@3.5.1/dist/jquery.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>

<script src="https://cdn.jsdelivr.net/npm/bootstrap@4.6.1/dist/js/bootstrap.bundle.min.js" integrity="sha384-fQybjgWLrvvRgtW6bFlB7jaZrFsaBXjsOMm/tB9LTS58ONXgqbR9W8oWht/amnpF" crossorigin="anonymous"></script>

</body>

</html>