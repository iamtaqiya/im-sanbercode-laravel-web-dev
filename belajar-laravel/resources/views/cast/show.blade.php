@extends("layouts.master")

@section("judul")
    Detail Pemain Film
@endsection

@section("content")

<h1 class="text-primary">{{$castData->nama}}</h1>
<p>{{$castData->umur}}</p>
<p>{{$castData->bio}}</p>

<a href="/cast" class="btn btn-secondary btn-sm">Kembali</a>
@endsection