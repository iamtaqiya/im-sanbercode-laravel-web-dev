@extends("layouts.master")

@section("judul")
    Edit Pemain Film
@endsection

@section("content")

<form action="/cast/{{$castData->id}}" method="POST">
    @csrf
    @method('PUT')

    <div class="form-group">
        <label>Nama Pemain</label>
        <input type="text" name="nama" value="{{$castData->nama}}" class="form-control">
    </div>

    @error('nama')
    <div class="alert alert-danger">{{ $message }}</div>
    @enderror

    <div class="form-group">
        <label>Umur Pemain</label>
        <input type="number" name="umur" value="{{$castData->umur}}" class="form-control">
    </div>

    @error('umur')
    <div class="alert alert-danger">{{ $message }}</div>
    @enderror

    <div class="form-group">
        <label>Bio Pemain</label>
        <textarea name="bio" class="form-control" cols="30" rows="10">{{$castData->bio}}</textarea>
    </div>

    @error('bio')
    <div class="alert alert-danger">{{ $message }}</div>
    @enderror
    
    <button type="submit" class="btn btn-primary">Edit</button>
</form>

@endsection