@extends("layouts.master")

@section("judul")
    Para Pemain Film
@endsection

@section("content")

<a href="/cast/create" class="btn btn-primary btn-sm my-3">Tambah</a>

<table class="table">
    <thead class="thead-dark">
        <tr>
            <th scope="col">#</th>
            <th scope="col">Nama Pemain Film</th>
            <th scope="col">Action</th>
        </tr>
    </thead>
    
    <tbody>
        @forelse ($cast as $key => $item)
            <tr>
                <th scope="row">{{$key + 1}}</th>
                <td>{{$item->nama}}</td>
                <td>
                    <form action="/cast/{{$item->id}}" method="POST">
                    @csrf
                    @method('DELETE')
                    <a href="/cast/{{$item->id}}" class="btn btn-info btn-sm mx-1">Detail</a>
                    <a href="/cast/{{$item->id}}/edit" class="btn btn-warning btn-sm mx-1">Edit</a>
                    <input type="submit" class="btn btn-danger btn-sm mx-1" value="Delete">
                    </form>
                </td>
            </tr>
        @empty
            <tr>
                <td>Belum Ada Pemain Film</td>
            </tr>
        @endforelse
    </tbody>
  </table>

@endsection