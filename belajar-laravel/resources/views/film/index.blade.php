@extends("layouts.master")

@section("judul")
    Semua Film
@endsection

@section("content")

{{-- kalo @auth @endauth hilang, nanti cuma muncul tombol tapi tetep ke login--}}
@auth
<a href="/film/create" class="btn btn-primary my-3">Tambah</a>
@endauth

<div class="row">
    @forelse ($film as $item)
        <div class="col-4">
            <div class="card">
                <img src="{{asset('Poster/'. $item->poster)}}" class="card-img-top" height="200px" alt="{{$item->judul}}">
                <div class="card-body">
                    <h5>{{$item->judul}}</h5>
                    <p class="card-text">{{Str::limit($item->ringkasan, 50)}}</p>
                    <a href="/film/{{$item->id}}" class="btn btn-primary btn-block btn-sm">More</a>

                    {{-- kalo @auth @endauth hilang, nanti cuma muncul tombol tapi tetep ke login--}}
                    @auth 
                    <div class="row my-2">
                        <div class="col">
                            <a href="/film/{{$item->id}}/edit" class="btn btn-warning btn-block btn-sm">Edit</a>
                        </div>
                        <div class="col">
                            <form action="/film/{{$item->id}}" method="POST">
                                @csrf
                                @method('DELETE')
                                <input type="submit" class="btn btn-danger btn-block btn-sm" value="Delete">
                            </form>
                        </div>
                    </div>
                    @endauth

                </div>
              </div>
        </div>    
    @empty
        <div>
            <h3>Tidak Ada Film</h3>
        </div>
    @endforelse
    
</div>
@endsection