@extends("layouts.master")

@section("judul")
    Detail Film
@endsection

@section("content")

<img src="{{asset('/Poster/'. $film->poster)}}" width="100%" height="300px" alt="">

<h1 class="text-info my-2">{{$film->judul}}</h1>
<p>{{$film->ringkasan}}</p>

<a href="/film" class="btn btn-secondary btn-sm">Kembali</a>
@endsection