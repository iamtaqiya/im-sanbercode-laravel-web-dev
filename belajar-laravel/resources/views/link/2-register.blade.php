@extends("layouts.master")

@section("judul")
Sign Up Form
@endsection
    
@section("content")
    <form action="/3-welcome" method="post">
        {{-- setiap form harus dikasih csrf untuk proteksinya --}}
        @csrf
        <label for="">First name:</label> <br>
        <input type="text" name="fname"> <br><br>

        <label for="">Last name:</label> <br>
        <input type="text" name="lname"> <br><br>

        <label for="">Gender:</label> <br>
        <input type="radio">Male <br>
        <input type="radio">Female <br>
        <input type="radio">Other <br><br>

        <label for="">Nationality:</label> <br>
        <select name="nationalities" id="">
            <option value="indonesian">Indonesian</option>
            <option value="singaporean">Singaporean</option>
            <option value="malaysian">Malaysian</option>
            <option value="australian">Australian</option>
        </select> <br><br>

        <label for="">Language Spoken:</label> <br>
        <input type="checkbox" name="language spoken" id="">Bahasa Indonesia <br>
        <input type="checkbox" name="language spoken" id="">English <br>
        <input type="checkbox" name="language spoken" id="">Other <br><br>

        <label for="">Bio:</label> <br>
        <textarea name="bio" id="" cols="30" rows="10"></textarea> <br>
        <input type="submit" value="Sign Up">
    </form>
@endsection