<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\AuthController;
use App\Http\Controllers\CastController;
use App\Http\Controllers\GenreController;
use App\Http\Controllers\GameController;
use App\Http\Controllers\FilmController;
use App\Http\Controllers\ProfilController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
| (endpoint)
*/

// Route::get('/', function () {
//     return view('welcome');
// });

// Tugas 12 = intro Laravel web statis
Route::get("/", [HomeController::class, "satu"]);

Route::get("/2-register", [AuthController::class, "dua"]);

Route::post("/3-welcome", [AuthController::class, "tiga"]);


// Tugas 13 = template blade Laravel
Route::get("/table", function() {
    return view("page.table");
});

Route::get("/data-tables", function() {
    return view("page.data-tables");
});


// AGAR HANYA BISA DIAKSES KALO USER SUDAH LOGIN
Route::group(['middleware' => ['auth']], function () {
    // Tugas 15 = CRUD Cast [pake Query Builder]
    // CREATE
    // Menampilkan form untuk membuat data pemain film baru
    Route::get("/cast/create", [CastController::class, 'create']);

    // Menyimpan data baru ke tabel Cast
    Route::post("/cast", [CastController::class, 'store']);

    // READ
    // Menampilkan list data para pemain film (boleh menggunakan table html atau bootstrap card)
    Route::get("/cast", [CastController::class, 'index']);

    // Menampilkan detail data pemain film dengan id tertentu
    Route::get("/cast/{id}", [CastController::class, 'show']);

    // UPDATE
    // Menampilkan form untuk edit pemain film dengan id tertentu
    Route::get("/cast/{id}/edit", [CastController::class, 'edit']);

    // Menyimpan perubahan data pemain film (update) untuk id tertentu
    Route::put("/cast/{id}", [CastController::class, 'update']);

    // DELETE
    // Menghapus data pemain film dengan id tertentu
    Route::delete("/cast/{id}", [CastController::class, 'destroy']);


    // CRUD Genre [pake Query Builder]
    // CREATE
    // Menampilkan form untuk membuat data genre baru
    Route::get("/genre/create", [GenreController::class, 'create']);

    // Menyimpan data baru ke tabel Genre
    Route::post("/genre", [GenreController::class, 'store']);

    // READ
    // Menampilkan list data genre (boleh menggunakan table html atau bootstrap card)
    Route::get("/genre", [GenreController::class, 'index']);

    // Menampilkan detail data genre dengan id tertentu
    Route::get("/genre/{id}", [GenreController::class, 'show']);

    // UPDATE
    // Menampilkan form untuk edit genre dengan id tertentu
    Route::get("/genre/{id}/edit", [GenreController::class, 'edit']);

    // Menyimpan perubahan data genre (update) untuk id tertentu
    Route::put("/genre/{id}", [GenreController::class, 'update']);

    // DELETE
    // Menghapus data genre dengan id tertentu
    Route::delete("/genre/{id}", [GenreController::class, 'destroy']);


    //Fitur update profil
    Route::get('/profil', [ProfilController::class, 'index']);
    Route::put('/profil/{id}', [ProfilController::class, 'update']);
});


// CRUD Film [pake ORM]
Route::resource('film', FilmController::class);


// QUIZ PEKAN 3 = CRUD Game
// CREATE
// Menampilkan form untuk membuat data baru
Route::get("/game/create", [GameController::class, 'create']);

// Menyimpan data baru ke tabel Game
Route::post("/game", [GameController::class, 'store']);

// READ
// Menampilkan list data Game
Route::get("/game", [GameController::class, 'index']);

// Menampilkan detail data Game dengan id tertentu
Route::get("/game/{id}", [GameController::class, 'show']);

// UPDATE
// Menampilkan form untuk edit Game dengan id tertentu
Route::get("/game/{id}/edit", [GameController::class, 'edit']);

// Menyimpan perubahan data Game (update) untuk id tertentu
Route::put("/game/{id}", [GameController::class, 'update']);

// DELETE
// Menghapus data Game dengan id tertentu
Route::delete("/game/{id}", [GameController::class, 'destroy']);

// dari php artisan ui bootstrap --auth
Auth::routes();

// Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
