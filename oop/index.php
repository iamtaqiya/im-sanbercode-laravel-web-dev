<?php
// Release 0

require_once ("Animal.php");
require_once ("Frog.php");
require_once ("Ape.php");
    
$sheep = new Animal("shaun");

echo "Animal's name = " . $sheep->name . "<br>"; // "shaun"
echo "Their legs are = $sheep->legs <br>"; // 4
echo "Cold blooded? $sheep->cold_blooded <br><br>"; // "no"

// NB: Boleh juga menggunakan method get (get_name(), get_legs(), get_cold_blooded())
    
    
// Release 1
// Kodok buduk
$Frog = new Frog("buduk");
    
echo "Animal's name = " . $Frog->name . "<br>"; // "buduk"
echo "Legs = " . $Frog->legs . "<br>"; // 4
echo "Cold blooded = " . $Frog->cold_blooded. "<br>"; // "no"
$Frog->jump();
echo "<br><br>";
    
// Ape kera sakti
$sunggokong = new Ape("kera sakti");

echo "Animal's name = " . $sunggokong->name . "<br>"; // "kera sakti"
echo "Legs = " . $sunggokong->legs . "<br>"; // 4
echo "Cold blooded = " .$sunggokong->cold_blooded. "<br>"; // "no"
$sunggokong->yell(). "<br><br>";
?>
